package com.cnam.project.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by I324896 on 25/04/2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Document
public class Station {

    @Id
    private Long id;

    private String nameStation;

    @JsonIgnore
    @DBRef
    private Network network;

    public Station(String nameStation) {
        this.nameStation = nameStation;
    }
}
