package com.cnam.project.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;


import java.util.List;

/**
 * Created by I324896 on 25/04/2017.
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class OptimalItinerary {
    

    private int timeTravel;

    private List<Station> listOrderedStations;

    public void addStation(Station station){
        listOrderedStations.add(station);
    }
}
