package com.cnam.project.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

/**
 * Created by I324896 on 02/05/2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class StationTravel {

    @Id
    private Long id;

    @DBRef
    private Station departure;
    @DBRef
    private Station arrival;

    private int timeTravel;

    public StationTravel(Station departure, Station arrival, int timeTravel) {
        this.departure = departure;
        this.arrival = arrival;
        this.timeTravel = timeTravel;
    }
}
