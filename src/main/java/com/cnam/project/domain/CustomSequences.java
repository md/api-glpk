package com.cnam.project.domain;

/**
 * Created by Max on 27/04/2017.
 */
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "customSequences")
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class CustomSequences {
    @Id
    private String id;
    private Long seq;
}
