package com.cnam.project.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;


import java.util.List;
import java.util.Map;

/**
 * Created by I324896 on 25/04/2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Document
public class Network {

    @Id
    private Long id;

    private String name;
    @DBRef
    private List<Station> listStation;

    @DBRef
    private List<StationTravel> listTimeTravel;

    public Network(String name, List<Station> listStation) {
        this.name = name;
        this.listStation = listStation;
    }
}
