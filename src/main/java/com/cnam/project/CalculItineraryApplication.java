package com.cnam.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalculItineraryApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalculItineraryApplication.class, args);
	}
}
