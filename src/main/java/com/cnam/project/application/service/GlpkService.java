package com.cnam.project.application.service;


import com.cnam.project.application.repository.StationRepository;
import com.cnam.project.domain.Network;
import com.cnam.project.domain.OptimalItinerary;
import com.cnam.project.domain.Station;
import com.cnam.project.domain.StationTravel;
import org.gnu.glpk.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;


/**
 * Created by I324896 on 25/04/2017.
 */
@Service
public class GlpkService implements GlpkCallbackListener, GlpkTerminalListener {
    private static String MOD_FILE_NAME = "problem.mod";
    private boolean hookUsed = false;
    private OptimalItinerary optimalItinerary = new OptimalItinerary();

    @Autowired
    StationTravelService stationTravelService;

    @Autowired
    StationService stationService;

    @Autowired
    NetworkService networkService;

    @Autowired
    SequenceService sequenceService;

    public OptimalItinerary populate() throws IOException {

        List<StationTravel> lTravel = new ArrayList<>();
        Network network = new Network();
        network.setId(sequenceService.getNextSequence("networks"));
        network.setName("test-network");
        List<Station> listStation = new ArrayList<>();
        Station st;
        for (int i =1; i<=8;i++){
            st = new Station();
            st.setId(sequenceService.getNextSequence("stations"));
            st.setNameStation("station"+i);
            st.setNetwork(network);
            listStation.add(st);
            stationService.add(st);
        }


        network.setListStation(listStation);
        StationTravel sTravel1 = new StationTravel(listStation.get(0),listStation.get(1),1);
        StationTravel sTravel2 = new StationTravel(listStation.get(0),listStation.get(2),8);
        StationTravel sTravel3 = new StationTravel(listStation.get(0),listStation.get(6),6);
        StationTravel sTravel4 = new StationTravel(listStation.get(1),listStation.get(3),2);
        StationTravel sTravel5 = new StationTravel(listStation.get(2),listStation.get(1),14);
        StationTravel sTravel6 = new StationTravel(listStation.get(2),listStation.get(3),10);
        StationTravel sTravel7 = new StationTravel(listStation.get(2),listStation.get(4),6);
        StationTravel sTravel8 = new StationTravel(listStation.get(2),listStation.get(5),19);
        StationTravel sTravel9 = new StationTravel(listStation.get(3),listStation.get(4),8);
        StationTravel sTravel10 = new StationTravel(listStation.get(3),listStation.get(7),13);
        StationTravel sTravel11 = new StationTravel(listStation.get(4),listStation.get(7),12);
        StationTravel sTravel12 = new StationTravel(listStation.get(5),listStation.get(4),7);
        StationTravel sTravel13 = new StationTravel(listStation.get(6),listStation.get(3),5);
        StationTravel sTravel14 = new StationTravel(listStation.get(7),listStation.get(5),4);
        StationTravel sTravel15 = new StationTravel(listStation.get(7),listStation.get(6),10);

        lTravel.add(sTravel1);
        lTravel.add(sTravel2);
        lTravel.add(sTravel3);
        lTravel.add(sTravel4);
        lTravel.add(sTravel5);
        lTravel.add(sTravel6);
        lTravel.add(sTravel7);
        lTravel.add(sTravel8);
        lTravel.add(sTravel9);
        lTravel.add(sTravel10);
        lTravel.add(sTravel11);
        lTravel.add(sTravel12);
        lTravel.add(sTravel13);
        lTravel.add(sTravel14);
        lTravel.add(sTravel15);

        network.setListTimeTravel(lTravel);

        stationTravelService.saveFromList(network.getListTimeTravel());
        networkService.add(network);
        List<Station> lSt = stationService.findByNetwork(network);
        return FoundItinerary(1L,1L,6L);



        /*Map<String,Integer> mapTimeStations = new LinkedHashMap<>();
        mapTimeStations.put("1-2",1);
        mapTimeStations.put("1-3",8);
        mapTimeStations.put("1-7",6);
        mapTimeStations.put("2-4",2);
        mapTimeStations.put("3-2",14);
        mapTimeStations.put("3-4",10);
        mapTimeStations.put("3-5",6);
        mapTimeStations.put("3-6",19);
        mapTimeStations.put("4-5",8);
        mapTimeStations.put("4-8",13);
        mapTimeStations.put("5-8",12);
        mapTimeStations.put("6-5",7);
        mapTimeStations.put("7-4",5);
        mapTimeStations.put("8-6",4);
        mapTimeStations.put("8-7",10);

        */

        //generateProblem(network,listStation.get(0),listStation.get(5),listStation.get(listStation.size()-1).getId());


    }




    public OptimalItinerary FoundItinerary(Long networkId, Long departureId, Long arrivalId) throws IOException {


        Network network = networkService.get(networkId);
        Station departure = stationService.get(departureId);
        Station arrival = stationService.get(arrivalId);
        List<Station> listStation = network.getListStation();

        generateProblem(network,departure,arrival,listStation.get(listStation.size()-1).getId());
        String[] nomeArquivo = new String[2];
        nomeArquivo[0] = MOD_FILE_NAME;

        System.out.println(nomeArquivo[0]);
        GLPK.glp_java_set_numeric_locale("C");
        System.out.println(nomeArquivo[0]);
        optimalItinerary.setListOrderedStations(solve(nomeArquivo, listStation));
        return optimalItinerary;

    }

    public void generateProblem (Network network, Station departure, Station arrival, Long lastStationOfNetwork) throws IOException {

        StringBuilder sb = new StringBuilder();
        List<String> lineLP = new ArrayList<>();
        Long firstNode = network.getListStation().get(0).getId();

        lineLP.add("param n, integer, > 0;\n");
        lineLP.add("set E, within {i in "+firstNode+"..n, j in "+firstNode+"..n};\n\n");
        lineLP.add("param c{(i,j) in E};\n\n");
        lineLP.add("param s, in {"+firstNode+"..n};\n\n");
        lineLP.add("param t, in {"+firstNode+"..n};\n\n");
        lineLP.add("var x{(i,j) in E}, >= 0;\n\n");
        lineLP.add("s.t. r{i in "+firstNode+"..n}: sum{(j,i) in E} x[j,i] + (if i = s then 1) =\n" +
                "                   sum{(i,j) in E} x[i,j] + (if i = t then 1);\n\n");
        lineLP.add("minimize Z: sum{(i,j) in E} c[i,j] * x[i,j];\n\n");
        lineLP.add("data;\n\n");
        lineLP.add("param n := " + lastStationOfNetwork + ";\n\n");
        lineLP.add("param s := " + departure.getId() + ";\n\n");
        lineLP.add("param t := " + arrival.getId() + ";\n\n");

        sb.append("param : E :   c := ");

        for (StationTravel st: network.getListTimeTravel()){
            sb.append(st.getDeparture().getId());
            sb.append(" ");
            sb.append(st.getArrival().getId());
            sb.append(" ");
            sb.append(st.getTimeTravel());
            sb.append(" ");
        }

        sb.append(";\n\n");
        lineLP.add(sb.toString());
        lineLP.add("end;");
        Path file = Paths.get(MOD_FILE_NAME);
        Files.write(file, lineLP, Charset.forName("UTF-8"));
    }


    public List<Station> solve(String[] arg,List<Station> listStation) {
        glp_prob lp = null;
        glp_tran tran;
        glp_iocp iocp;

        String fname;
        int skip = 0;
        int ret;

        // listen to callbacks
        GlpkCallback.addListener(this);
        // listen to terminal output
        GlpkTerminal.addListener(this);

        fname = arg[0];

        lp = GLPK.glp_create_prob();
        System.out.println("Problem created");
        tran = GLPK.glp_mpl_alloc_wksp();
        ret = GLPK.glp_mpl_read_model(tran, fname, skip);
        if (ret != 0) {
            GLPK.glp_mpl_free_wksp(tran);
            GLPK.glp_delete_prob(lp);
            throw new RuntimeException("Model file not found: " + fname);
        }

        // generate model
        GLPK.glp_mpl_generate(tran, null);
        // build model
        GLPK.glp_mpl_build_prob(tran, lp);
        // set solver parameters
        iocp = new glp_iocp();
        GLPK.glp_init_iocp(iocp);
        iocp.setPresolve(GLPKConstants.GLP_ON);

        // do not listen to output anymore
        GlpkTerminal.removeListener(this);
        // solve model
        ret = GLPK.glp_intopt(lp, iocp);
        // postsolve model
        if (ret == 0) {
            GLPK.glp_mpl_postsolve(tran, lp, GLPKConstants.GLP_MIP);
        }

        return write_mip_solution(lp,listStation);
        // free memory
        /*GLPK.glp_mpl_free_wksp(tran);
        GLPK.glp_delete_prob(lp);

        // do not listen for callbacks anymore
        GlpkCallback.removeListener(this);

        // check that the hook function has been used for terminal output.
        if (!hookUsed) {
            System.out.println("Error: The terminal output hook was not used.");
            System.exit(1);
        }*/


    }

    public List<Station> write_mip_solution(glp_prob lp,List<Station> listStation) {
        int i;
        int n;
        String name;
        Double val;
        List<Station> optimalStations = new ArrayList<>();

        name = GLPK.glp_get_obj_name(lp);
        val = GLPK.glp_mip_obj_val(lp);
        System.out.println("Durée du parcours (" + name + ")" + " : " + val.intValue());
        optimalItinerary.setTimeTravel(val.intValue());
        n = GLPK.glp_get_num_cols(lp);

        for (i = 1; i <= n; i++) {
            name = GLPK.glp_get_col_name(lp, i);
            val = GLPK.glp_mip_col_val(lp, i);
            if (val > 0.0) {
                System.out.println("Coordonnées " + name + " = " + val);
                List<String> arrayName = Arrays.asList(name.replace("[","").replace("]","").replace("x","").split(","));

                //System.out.println(arrayName.get(0) + " + " + arrayName.get(1));
                for (Station s : listStation){
                    if (s.getId() == Long.valueOf(arrayName.get(0)) || s.getId() == Long.valueOf(arrayName.get(1))){
                        if (!optimalStations.contains(s)){
                            System.out.println(s.getId());
                            optimalStations.add(s);
                        }

                    }
                }

            }
        }
        return optimalStations;
    }

    @Override
    public boolean output(String str) {
        hookUsed = true;
        System.out.print(str);
        return false;
    }

    @Override
    public void callback(glp_tree tree) {
        int reason = GLPK.glp_ios_reason(tree);
        if (reason == GLPKConstants.GLP_IBINGO) {
            System.out.println("Better solution found");
        }
    }
}
