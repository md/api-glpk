package com.cnam.project.application.service;

import com.cnam.project.application.repository.StationTravelRepository;
import com.cnam.project.domain.StationTravel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Max on 11/05/2017.
 */
@Service
public class StationTravelService {

    @Autowired
    StationTravelRepository stationTravelRepository;

    @Autowired
    SequenceService sequenceService;

    public void saveFromList(List<StationTravel> lStationTravel){
        for (StationTravel st : lStationTravel){
            st.setId(sequenceService.getNextSequence("stationTravel"));
            stationTravelRepository.save(st);
        }
    }

    public List<StationTravel> fetchAll (){
        return stationTravelRepository.findAll();
    }
}