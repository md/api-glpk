package com.cnam.project.application.service;

import com.cnam.project.application.repository.NetworkRepository;
import com.cnam.project.domain.Network;
import com.cnam.project.domain.Station;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Max on 27/04/2017.
 */
@Service
public class NetworkService {

    @Autowired
    NetworkRepository networkRepository;

    @Autowired
    SequenceService sequenceService;

    public void add(Network network){
        if (network.getId() == null){
            network.setId(sequenceService.getNextSequence("networks"));
        }
        networkRepository.save(network);
    }

    public Network get(Long id){
        return networkRepository.findOne(id);
    }

    public void delete(Network network){
        networkRepository.delete(network);
    }

    public void delete(Long id){
        networkRepository.delete(id);
    }
}
