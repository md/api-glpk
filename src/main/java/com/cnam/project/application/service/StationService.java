package com.cnam.project.application.service;

import com.cnam.project.application.repository.StationRepository;
import com.cnam.project.domain.Network;
import com.cnam.project.domain.Station;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.nio.ch.Net;

import java.util.List;

/**
 * Created by Max on 27/04/2017.
 */
@Service
public class StationService {

    @Autowired
    StationRepository stationRepository;

    @Autowired
    SequenceService sequenceService;

    public void add(Station station){
        stationRepository.save(station);
    }

    public void addFromNetwork(Network network, List<Station> listStation) {
        System.out.println(listStation.size());
        for (Station s : listStation){
            System.out.println(s.getNameStation());
            s.setId(sequenceService.getNextSequence("stations"));
            s.setNetwork(network);
            stationRepository.save(s);
        }
    }

    public void addMultiple(List<Station> listStation){
        for (Station s : listStation){
            System.out.println(s.getNameStation());
            s.setId(sequenceService.getNextSequence("stations"));
            stationRepository.save(s);
        }

    }

    public void setNetwork(Network network, Station station){
        Station updateStation = get(station.getId());
        updateStation.setNetwork(network);
        add(updateStation);

    }

    public void setNetworkToMany(Network network, List<Station> listStation){

        for (Station s :listStation){
            setNetwork(network,s);
        }

    }

    public Station get(Long id){
        return stationRepository.findOne(id);
    }
    public List<Station> getAll(){
        return stationRepository.findAll();
    }

    public List<Station> findByNetwork(Network network){
        return stationRepository.findByNetwork(network);
    }

    public void delete(Station station){
        stationRepository.delete(station);
    }

    public void delete(Long id){
        stationRepository.delete(id);
    }

    public void deleteByNetwork(Network network){
        for (Station s: findByNetwork(network)){
            stationRepository.delete(s);
        }
    }
}
