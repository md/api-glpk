package com.cnam.project.application.api;

import com.cnam.project.application.service.GlpkService;
import com.cnam.project.domain.OptimalItinerary;
import com.cnam.project.domain.Station;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Created by donge on 26/04/2017.
 */

@RestController
@RequestMapping(path = "api/v1/glpk")
public class GLPKController {

    @Autowired
    private GlpkService glpkService;

    @RequestMapping(value = "/populate",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public OptimalItinerary PopulateMongo() throws IOException {
        return glpkService.populate();
    }


    @RequestMapping(value = "/solve/{networkId}/{departureId}/{arrivalId}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public OptimalItinerary Solve(@PathVariable Long networkId,@PathVariable Long departureId, @PathVariable Long arrivalId ) throws IOException {
        return glpkService.FoundItinerary(networkId,departureId,arrivalId);

    }

}
