package com.cnam.project.application.api;

import com.cnam.project.application.service.StationService;
import com.cnam.project.domain.Network;
import com.cnam.project.domain.Station;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Max on 28/04/2017.
 */
@RestController
@RequestMapping(value = "api/v1/station")
public class StationController {

    @Autowired
    StationService stationService;
    @RequestMapping(value = "/", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Station> findAll(){

        return stationService.getAll();
    }

    @RequestMapping(value = "/test", method = RequestMethod.POST)
    public void test(@RequestBody Network network){
        for (Station s: network.getListStation()){
            System.out.println(s.getNameStation());
        }


    }
}
