package com.cnam.project.application.api;

import com.cnam.project.application.service.NetworkService;
import com.cnam.project.application.service.StationService;
import com.cnam.project.domain.Network;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Max on 27/04/2017.
 */
@RequestMapping(path = "api/v1/network")
@RestController
public class NetworkController {

    @Autowired
    NetworkService networkService;

    @Autowired
    StationService stationService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> create(@RequestBody Network network){
        try {

            //stationService.addFromNetwork(network,network.getListStation());
            stationService.addMultiple(network.getListStation());
            networkService.add(network);
            stationService.setNetworkToMany(network,network.getListStation());
            //stationService.addFromNetwork(network,network.getListStation());
            return new ResponseEntity<>(HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> find(@RequestParam Long id){

        try {
            Network network = networkService.get(id);
            return new ResponseEntity<>(network,HttpStatus.FOUND);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable Long id) {
        try {
            stationService.deleteByNetwork(networkService.get(id));
            networkService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


}
