package com.cnam.project.application.repository;

import com.cnam.project.domain.Network;
import com.cnam.project.domain.Station;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by Max on 27/04/2017.
 */
public interface StationRepository extends MongoRepository<Station, Long> {

    public Station findByNameStation (String name);

    public List<Station> findByNetwork(Network network);


}
