package com.cnam.project.application.repository;

import com.cnam.project.domain.Station;
import com.cnam.project.domain.StationTravel;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by Max on 11/05/2017.
 */
public interface StationTravelRepository extends MongoRepository<StationTravel, Long> {
}
