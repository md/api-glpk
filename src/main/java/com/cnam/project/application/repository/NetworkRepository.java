package com.cnam.project.application.repository;

import com.cnam.project.domain.Network;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by Max on 27/04/2017.
 */
public interface NetworkRepository extends MongoRepository<Network,Long> {

    public Network findByName(String name);
}
